package com.x4pass.kotlinboot.controllers

import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Description.
 *
 * @auth xingbing.lai@gmail.com
 * @version 0.0.1 Datetime: 17-5-26下午10:31.
 */
@Controller
@RequestMapping("/")
class IndexController{

    @RequestMapping("/index")
    @ResponseBody
    fun index():String{
        return "hello kotlin!"
    }

    @RequestMapping("/test")
    fun test(modelMap: ModelMap):String{
        modelMap.addAttribute("name", "Jona")
        return "test"
    }
}
